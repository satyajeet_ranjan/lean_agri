from django.http import HttpResponse
from django.shortcuts import render
from .models import FarmData,Farmer,ScheduleData
from django.views.generic import View

# Create your views here.

def calculate(request,id):
    farmer_data_detais = FarmData.objects.get(farmer_id=id)
    farm_data_id = farmer_data_detais.id
    schedule_data_details = ScheduleData.objects.filter(farmdata_id=farm_data_id)
    fertilizers_details = {}
    for schedule_data in schedule_data_details:
        print(schedule_data.fertilizer)
        fertilizers_details[schedule_data.fertilizer]=schedule_data.quantity
    html =""
    html += "<h1 align=center> There are total of " + str(len(fertilizers_details)) + " fertilizers used.</h1>"
    html += "<table border = 5><tr><th>Fertilizer name</th><th>Fertilizer quantity(In Mili-litres)</th><th>Calculate cost</th></tr>"


    for fert_name,fert_quantity in fertilizers_details.items():
        html += "<td> "+ fert_name +"</td><td>" + fert_quantity +"</td><td>" + "<a href='/firstapp/calculatecost/"+fert_quantity +"/"+fert_name+"/'>calculate</a>"+"</td></tr>"
    return HttpResponse(html)

def calculate_cost(request,qu,name):
    html = "<h1 align=center> Find the cost of :"+ name+"</h1>"
    html += "<input type='text' id='cost' placeholder='Enter the cost for 1 litre'/>" \
           "<button onclick=calculate()>calculate</button>" \
            "<p>The Cost of "+qu+" Ml of "+name+" is :</p>"\
            "<p id='result'></p>"
    html += "<script>" \
            "function calculate(){" \
            "var cost = document.getElementById('cost').value;" \
            "var final_price = (cost/1000)*"+qu+";"\
            "" \
            "document.getElementById('result').innerHTML = 'Rs: '+final_price;}</script>"
    return HttpResponse(html)

def show_request(request):
    html= "<h1>Enter a crop name </h1><div class='form'>" \
                "<input type='text' placeholder='Please enter the crop name' id='crop_name'>" \
                "<br><button onclick='myfunc()'>Search</button>" \
                "<script>" \
                    "function myfunc(){" \
                    "var search_data = document.getElementById('crop_name').value;" \
                    "" \
                    "window.location.assign(window.location.href +search_data)}" \
                "</script>" \
          "</div>"
    return HttpResponse(html)
def Show_data(request,c):
    farm_data  = FarmData.objects.filter(cropGrown=c)
    print (farm_data)
    famers_id = []
    html = "<h1 align=center>Farmers who are growing "+c+" crops</h1>" \
           "<table align=center border=5><tr><th>Farmer name</th><th>Phone Number</th><th>Language</th><th>Cost of fertilizer used</th></tr>"
    for data in farm_data:

        famers_id.append(data.farmer_id)
        # return HttpResponse(str(data.farmer_id))
    if(len(famers_id)==0):
        html+="</table><h2 align=center>Currently no farmers are growing "+c +"  crops</h2>"
    for farmer_id in famers_id:
        farmer_details = Farmer.objects.get(pk=farmer_id)
        html += "<tr><td>"+ farmer_details.name+"</td><td>"+farmer_details.phone_number+"</td><td>"+ farmer_details.language +"<td>"+ "<a href='/firstapp/calculate/"+str(farmer_details.id)+"'>Find</a>" +"</td></tr>"

    html +="</table>"
    return HttpResponse(html)



def index(request):
    html = ''
    #get all farmers
    farmers = Farmer.objects.all()

    for farmer in farmers:
        farmdata = FarmData.objects.get(pk=farmer.pk)
        scheduledata = ScheduleData.objects.filter(farmdata_id=farmdata.pk)
        # print(scheduledata)
        for scheduledat in scheduledata:
            print(farmdata.cropGrown)
            import datetime
            print("sowing date is "+ str(farmdata.sowingDate))
            time_delta = datetime.date.today() - farmdata.sowingDate
            remainingdays =  (farmdata.sowingDate + datetime.timedelta(days=int(scheduledat.days_after_sowing)))-farmdata.sowingDate
            # print(remainingdays.days==1)
            if(remainingdays==1):
                print(scheduledat.fertilizer +" today ")
            else:
                print("sorry no schedules")
        schedules =""
        #print(scheduledata)


        html += '<ul type="circle"><li>' + farmer.name + "</ul><ul>" + farmdata.area +"square_meter" + farmdata.cropGrown +" crop" +str(farmdata.sowingDate) + farmdata.village+" " + '</li></ul>'
        print (farmer.name,farmer.pk,farmer.phone_number)



    return HttpResponse(html)

