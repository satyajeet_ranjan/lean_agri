from django.urls import path
from . import views



urlpatterns = [
    path('', views.index, name="newApp"),
    path('cropdetails/<int:c>/',views.Show_data,name="Show_data"),
    path('cropdetails/<c>/',views.Show_data,name="Show_data"),
    path('cropdetails/',views.show_request),
    path('firstapp/calculate/<int:id>',views.calculate),
    path('firstapp/calculatecost/<qu>/<name>/',views.calculate_cost),
]
