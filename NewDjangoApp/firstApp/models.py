from django.db import models

# Create your models here.

class Farmer(models.Model):
    name = models.CharField(max_length=30)
    phone_number = models.CharField(max_length=15)
    language = models.CharField(max_length=10)
    def __str__(self):
        return self.name
class FarmData(models.Model):
    farmer = models.ForeignKey(Farmer,on_delete=models.CASCADE)
    area = models.CharField(max_length=20)
    village = models.CharField(max_length=30)
    cropGrown = models.CharField(max_length=10)
    sowingDate = models.DateField(max_length=10)
    def __str__(self):
        return self.village + " "+ self.cropGrown
class ScheduleData(models.Model):
    farmdata = models.ForeignKey(FarmData,on_delete=models.CASCADE)
    days_after_sowing = models.CharField(max_length=10)
    fertilizer = models.CharField(max_length=100)
    quantity = models.CharField(max_length=10)
    def __str__(self):
        return str(self.fertilizer) + " after "+ str(self.days_after_sowing)+" days of sowing"


