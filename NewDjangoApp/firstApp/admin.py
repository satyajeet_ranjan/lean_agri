from django.contrib import admin
from .models import FarmData,Farmer,ScheduleData

# Register your models here.

admin.site.register(Farmer)
admin.site.register(FarmData)
admin.site.register(ScheduleData)
